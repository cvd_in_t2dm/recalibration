# Sample recalibration procedure

The provide code snippets allows the user to recalibrate a risk prediction rule
 to a local (patient) population.
This specific implementation estimates a recalibration slope and intercept,
 to scale and translate the original score to better reflect the observed risk.

Additional code is provided, using the recalibrated prediction rule to predict
subjects risk.

Dziopa et al (add link when published), provides an example of recalibrating
22 cardiovascular risk prediction rules in type 2 diabetes patients.
As shown, recalibration can be used to repurpose risk prediction rules to
closely related, but distinct, outcomes.
For example, Dziopa et al, recalibrated coronary heart disease (CHD) scores,
to accurately predict heart failure (HF).

## Required data

* [required] A training sample is needed with information on a subjects risk
 (column name `risk`), and the observed outcome (column name: `outcome`)
* [optional] A testing sample containing the same information can be used to
calculate performance of the recalibrated score
(e.g., informally using the provided code to generate a calibration plot, or
more formally using calibration and discrimination statistics)

## Usage

### Recalibration - calculate updated intercept and slope

The `recalibration` function takes historical data and returns an intercept
and slope estimate.

* Input: `data` a n by p dataframe, with rows: subjects, and  columns:
`risk` and `outcome`.
* Output: `coefs_updates` contains a 2 element vector with intercept and slope.

```R
coefs_updates <- recalibration(data)
```

### Risk prediction using the recalibrated risk prediction rule

The `prediction_recal` function return an estimate of a subjects risk -
should not be used on the same recaibration data.

Input:

* `data` a n by p dataframe, with rows: subjects, and  columns: 'risk'.
* `coefs_updates` the 2-element vector with [1] the updated intercept and [2]
the slope.

Output:

* A n by 1 dataset with a subjects (rows) predicted risk

```R
data$update_risk <- prediction_recal(data, coefs_updates)
```

### Example

`Example.R` Includes a further example of how to apply these functions using
simulated data, including a calibration plot function.

![Sample calibration plot](/images/sample_calibration_plot.png?raw=true)

### Installation guidelines

`installation.R` Includes information about the required libraries to be installed.


>These scripts are made available under a CC BY-NC licence.
>Absolutely no guarantee or warranty is provided.
>We encourage and activly support implementations in other programming languages
>on the condition that these are made available under the same lincence.

