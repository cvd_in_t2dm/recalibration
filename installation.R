#' Script for installing required packages to run the 'Example.R'.
#' 
#' Required libraries:
#' dplyr - data manipulation
#' ggplot2 - creating plots
#' Hmisc - functions for data analysis
required_packages <- c("dplyr", "ggplot2", "Hmisc")
new_packages <- required_packages[!(required_packages %in% installed.packages()[,"Package"])]
if(length(new_packages)) install.packages(new_packages)